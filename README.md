

# Configuración de proyecto

El proyecto se desarrolló en la versión **4.2** de **Android Studio**.


# Información de la aplicación

La aplicación se desarrolló para leer un servicio en la nuve llamado pokeApi, esta aplicación muestra en la vista principal una lista de 20 pokemones con su nombre y su imagen, deslizando hacia abajo automaticamente van apareciendo lo demás pokemones de 20 en 20 y si se selecciona un pokemon aparece la información de este.



# Consumo de Servicios

El consumo de servicios se realizó con la librería Retrofit



# Variables por entorno

En este caso no se configuraron variables de entorno ya que no se proporcionaron urls de entornos previos

Las URLs correspondientes a los consumos de los servicios se encuentran en el archivo:

  ApiServices.java


En este caso no se generaron keystores ya que no se solicitó que se generara el apk 
