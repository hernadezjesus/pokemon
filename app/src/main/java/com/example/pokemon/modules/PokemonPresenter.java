package com.example.pokemon.modules;

import android.util.Log;

public class PokemonPresenter implements  PokemonInteractor.CallbackService{
    private PokemonView pokemonView;
    private PokemonInteractor interactor;

    public PokemonPresenter(PokemonView pokemonView) {
        Log.i("entrastatus","Hola");
        this.pokemonView = pokemonView;
    }


    public void onCreate(){
        interactor = new PokemonInteractor(this);
        getPokemon();


    }

    public void onDestroy() {
        pokemonView = null;
    }

    private void getPokemon(){
        interactor.getPokemons();
    }

    @Override
    public void onResponse(Object o) {

        Log.d("Respuesta", "onResponse: "+o.toString());

    }

    @Override
    public void onFailure() {

    }
}
