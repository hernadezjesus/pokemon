package com.example.pokemon.modules;

import android.os.AsyncTask;
import android.os.Build;

import com.example.pokemon.models.PokemonResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonInteractor {


    private CallbackService callbackService;
    private ServiceTask serviceTask;
    private Boolean cancel = false;

    public PokemonInteractor(CallbackService callbackService) {
        this.callbackService = callbackService;
    }

    public Call<PokemonResponse> getPokemons() {
        serviceTask = new ServiceTask();
        serviceTask.execute();
        return null;
    }

    void fragmentDetached(Boolean aBoolean) {
        cancel = aBoolean;
        if (serviceTask != null && cancel) {
            serviceTask.cancel(true);
        }
    }

    interface CallbackService {
        void onResponse(Object o);
        void onFailure();
    }

    private class ServiceTask extends AsyncTask<Void, Void, PokemonResponse> {

        @Override
        protected PokemonResponse doInBackground(Void... voids) {
            Call<PokemonResponse> call = getPokemons();
            call.enqueue(new Callback<PokemonResponse>() {
                @Override
                public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {
                    if (response.body() != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        }
                        callbackService.onResponse(response.body());
                    } else {
                        //decifrar(String.valueOf(response.body()));
                        callbackService.onFailure();
                    }
                }

                @Override
                public void onFailure(Call<PokemonResponse> call, Throwable t) {

                    callbackService.onFailure();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(PokemonResponse response) {
            super.onPostExecute(response);
        }
    }
}
