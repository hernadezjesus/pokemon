package com.example.pokemon.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.pokemon.databinding.DialogInformationBinding;
import com.example.pokemon.ui.FragmentPokemon;
import com.example.pokemon.R;
import com.example.pokemon.models.Pokemon;
import com.example.pokemon.ui.dialogs.DialogInformation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<Pokemon> dataset;
    private ArrayList<Pokemon> OItems;
    private Context context;
    private View.OnClickListener listener;
    DialogInformation dialog;

    public Adapter(Context context) {
        this.context = context;
        OItems = new ArrayList<>();
        dataset = new ArrayList<>();
        dialog= new DialogInformation();
        Log.d("original1", dataset.toString());
        OItems.addAll(dataset);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pokemon p = dataset.get(position);
        holder.nombreTextView.setText(p.getName());
        Log.d("nombre", p.getName());
        Log.d("imagenesUrl", String.valueOf("https://pokeapi.co/api/v2/pokemon-form/" + p.getNumber() + ".png"));
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getNumber() + ".png")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.fotoImageView);

        holder.lLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.showDialogInformation(p.getNumber(),p.getName(),p.getUrl(),context);
            }
        });

    }

    public void filter(String strSearch) {
        Log.d("strSearch", strSearch);
        int lng = strSearch.length();
        Log.d("lng", String.valueOf(lng));
        if (lng == 0) {
            Log.d("rellena", "rellena");
            dataset.clear();
            Log.d("Original", OItems.toString());
            dataset.addAll(OItems);
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.d("Entra1", strSearch);
                dataset.clear();
                List<Pokemon> collect = OItems.stream()
                        .filter(i -> i.getName().toLowerCase().contains(strSearch.toLowerCase()))
                        .collect(Collectors.toList());



                dataset.addAll(collect);
            }
            else {
                Log.d("Entra2", strSearch);
                dataset.clear();
                for (Pokemon i : OItems) {
                    if (i.getName().toLowerCase().contains(strSearch)) {
                        dataset.add(i);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    @Override
    public void onClick(View v) {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView fotoImageView;
        private TextView nombreTextView;
        private LinearLayout lLayoutContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            fotoImageView = itemView.findViewById(R.id.fotoPokemon);
            nombreTextView = itemView.findViewById(R.id.nombrePokemon);
            lLayoutContainer = itemView.findViewById(R.id.lLayoutContainer);
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    public void adicionarListaPokemon(ArrayList<Pokemon> listaPokemon) {
        dataset.addAll(listaPokemon);
        OItems.addAll(dataset);
        notifyDataSetChanged();
    }
}
