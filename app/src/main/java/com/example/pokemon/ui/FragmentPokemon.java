package com.example.pokemon.ui;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pokemon.R;
import com.example.pokemon.databinding.FragmentPokemonBinding;
import com.example.pokemon.models.Pokemon;
import com.example.pokemon.models.PokemonResponse;
import com.example.pokemon.modules.PokemonPresenter;
import com.example.pokemon.modules.PokemonView;
import com.example.pokemon.utils.Adapter;
import com.example.pokemon.utils.Api;
import com.example.pokemon.utils.ApiService;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentPokemon extends Fragment implements PokemonView, SearchView.OnQueryTextListener {

    FragmentPokemonBinding binding;
    PokemonPresenter presenter;
    private RecyclerView recyclerView;
    private SearchView svSearch;
    private Adapter adapter;
    private int offset;
    private boolean flag;
    public static ProgressDialog progdialog;

    public FragmentPokemon() {
        // Required empty public constructor
    }

    public static FragmentPokemon newInstance() {
        FragmentPokemon fragment = new FragmentPokemon();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPokemonBinding.inflate(inflater,container,false);


        progdialog = new ProgressDialog(getContext());
        progdialog.setMessage("Obteniendo Datos...");
        //  progdialog.setCancelable(false);
        progdialog.show();
        recyclerView = binding.getRoot().findViewById(R.id.recyclerView);
        svSearch = binding.getRoot().findViewById(R.id.svSearch);
        adapter = new Adapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        obtenerDatos(offset);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                    if (flag) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {

                            flag = false;
                            offset += 20;
                            obtenerDatos(offset);
                        }
                    }
                }
            }
        });
        presenter = new PokemonPresenter(this);
        svSearch.setOnQueryTextListener(this);
        offset=0;
        flag=true;

        return binding.getRoot();
    }

    private void obtenerDatos(int offset) {
        progdialog.dismiss();
        ApiService service = Api.getApi();
        Call<PokemonResponse> pokemonRespuestaCall = service.getPokemons(20,offset);

        pokemonRespuestaCall.enqueue(new Callback<PokemonResponse>() {
            @Override
            public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {
                flag = true;
                if (response.isSuccessful()) {

                    PokemonResponse pokemonRespuesta = response.body();
                    ArrayList<Pokemon> listaPokemon = pokemonRespuesta.getResults();


                    /*for (int i=0; i<listaPokemon.size();i++){

                        Pokemon p = listaPokemon.get(i);
                        Log.i("pokedes", p.getName());
                    }*/

                    adapter.adicionarListaPokemon(listaPokemon);

                } else {
                    Log.e("Eroor", " onResponse: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonResponse> call, Throwable t) {
                //aptoParaCargar = true;
                Log.e("onFailure", " onFailure: " + t.getMessage());
            }
        });
    }

    @Override public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }


    @Override
    public void createStatus() {

        progdialog.dismiss();

        /*Log.i("statuspubli", "count "+status.size());
        StatusSlidePagerAdapter adapter = new StatusSlidePagerAdapter(getActivity().getSupportFragmentManager(), status);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.addOnPageChangeListener(this);

        adapterHeader = new StatusHeaderAdapter(getContext(), R.layout.Vie_Fragment_pokemon, status, this);

        binding.scrollViewHeader.setAdapter(adapterHeader, status);

        binding.scrollViewHeader.setCenter(0, adapterHeader);
        myTrace.stop();
        binding.scrollViewHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(Config.TAG, "click en scroll");

            }
        });*/

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //adapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return false;
    }
}