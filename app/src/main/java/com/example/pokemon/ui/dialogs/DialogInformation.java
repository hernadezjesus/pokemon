package com.example.pokemon.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.pokemon.R;
import com.example.pokemon.ui.FragmentPokemon;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class DialogInformation {

    Context context;

    public void showDialogInformation(final int id, final String name, final String url, Context context) {

        this.context = context;
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_information);
        Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imageView = dialog.findViewById(R.id.iVpokemon);
        ImageView imageViewGift = dialog.findViewById(R.id.imageView2);
        TextView textViewName = dialog.findViewById(R.id.textViewName);
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
        textViewName.setText(name);

        Glide.with(this.context).load("https://www.gifsanimados.org/data/media/1446/pokemon-imagen-animada-0082.gif").into(imageViewGift);

        dialog.show();


    }

    /*private void callCancelLeasingParkingSpace(String json){
        APIServiceVehicleBean service = API.getAPI().create(APIServiceVehicleBean.class);
        service.callCancelLeasingParkingSpace(json).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
                int code = response.code();
                switch(code){
                    case 400:
                        if(response.errorBody() != null){
                            try {
                                openExceptionDialog(1,code,response.errorBody().string());
                            } catch (IOException e) {
                                Log.e("IOException: ",e.toString());
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 401:
                        if(response.errorBody() != null){
                            try {
                                openExceptionDialog(2,code,response.errorBody().string());
                            } catch (IOException e) {
                                Log.e("IOException: ",e.toString());
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 500:
                        if(response.errorBody() != null){
                            try {
                                openExceptionDialog(3,code,response.errorBody().string());
                            } catch (IOException e) {
                                Log.e("IOException: ",e.toString());
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 200:
                        if(response.isSuccessful()){
                            if(response.body() != null){
                                Log.i("JSON", "response JSON: "+new Gson().toJson(response.body()) );
                                progressBar.viewProgressBarFalse();
                                try {
                                    JSONObject responseJsonObject = new JSONObject(new Gson().toJson(response.body()));
                                    JSONObject dtoResponse = responseJsonObject.getJSONObject("estadoRespuestaDTO");
                                    String description = dtoResponse.getString("descripcion");
                                    dialogRequestParkingSpaceSuccess(description);
                                }catch (Exception e){
                                    dialogRequestParkingSpaceFail(e.toString());
                                    Log.e("ERROR", "jsonObject: " + e);
                                }

                            }
                        }
                        break;
                }

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                progressBar.viewProgressBarFalse();
                Log.e("ERROR", "Post Send ERROR: " + t);
                dialogRequestParkingSpaceFail(t.getMessage());

            }
        });*/

}
